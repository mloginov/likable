<?php

namespace Likeable;

trait HasLikeable
{
    public function like(Likeable $likeable, $state = true)
    {
        if ($like = $likeable->likes()->whereMorphedTo('userable', $this)->first()) {
            $like->delete();
            return;
        }

        (new Like())->userable()->associate($this)
            ->likeable()->associate($likeable)
            ->fill([
                'is_liked' => $state
            ])
            ->save();
    }

    public function dislike(Likeable $likeable)
    {
        if ($dislike = $likeable->dislikes()->whereMorphedTo('userable', $this)->first()) {
            $dislike->delete();
            return;
        }
        $this->like($likeable, false);
    }

}
