<?php

namespace Likeable;

use Illuminate\Database\Eloquent\Relations\MorphMany;

interface Likeable
{
    public function likes(): MorphMany;
    public function dislikes(): MorphMany;
}
